# This is where all your solutions that you create will be run from
# Tracking progress here with this roadmap https://neetcode.io/roadmap
from neetcode_roadmap.stack import evaluate_reverse_polish_notation

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    evaluate_reverse_polish_notation.answer_result()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
# For more info on Python naming convention, use this as a reference
# https://softwareengineering.stackexchange.com/questions/308972/python-file-naming-convention
