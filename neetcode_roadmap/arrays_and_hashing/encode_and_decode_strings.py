# https://www.lintcode.com/problem/659/description I have to sign up for this, I'm not doing that so no way to test
# answer
# Solved in ???
# Leave notes for Mediums and Highs please
class Solution:
    """
        @param: strs: a list of strings
        @return: encodes a list of strings to a single string.
        """

    def encode(self, input_string_list):
        result_string = ""
        for string in input_string_list:
            result_string += str(len(string)) + "#" + string
        print(result_string)
        return result_string

    """
    @param: str: A string
    @return: decodes a single string to a list of strings
    """

    def decode(self, input_string):
        result_list = []

        encode_offset_enabled = False
        offset_string = ""
        offset_value = 0
        string_to_add = ""
        # There's an easier way to do this, but right now this is what it will be
        for i in range(len(input_string)):
            if not encode_offset_enabled and input_string[i] != "#":
                offset_string += input_string[i]
            elif input_string[i] == "#":
                offset_value = int(offset_string)
                encode_offset_enabled = True
            elif offset_value == 0:
                result_list.append(string_to_add)
                encode_offset_enabled = False
                offset_string = ""
                string_to_add = ""
                offset_string += input_string[i]
            else:
                offset_value -= 1
                string_to_add += input_string[i]

        result_list.append(string_to_add)

        return result_list


# write your code here


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    user_input = ["lint", "code", "love", "you"]
    expected = ["lint", "code", "love", "you"]

    # Setting up part 🏗
    encoded_string = Solution().encode(user_input)
    answer = Solution().decode(encoded_string)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
