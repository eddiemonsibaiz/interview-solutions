# https://leetcode.com/problems/valid-sudoku/
# Solved in 01:42:18
# Leave notes for Mediums and Highs please
from collections import defaultdict


class Solution:

    def isValidSudoku(self, board: list[list[str]]) -> bool:
        # This seems to be very helpful. defaultdict essentially gives you a set by default, which is great for this
        # problem
        rows = defaultdict(set)
        cols = defaultdict(set)
        # This will be using a key of a tuple, interesting right?
        block = defaultdict(set)

        for i in range(9):
            for j in range(9):
                if board[i][j] == '.':
                    continue

                if board[i][j] in rows[i]:
                    return False
                if board[i][j] in cols[j]:
                    return False
                if board[i][j] in block[(i // 3, j // 3)]:
                    return False

                rows[i].add(board[i][j])
                cols[j].add(board[i][j])
                block[(i // 3, j // 3)].add(board[i][j])

        return True

    def isValidSudokuOriginal(self, board: list[list[str]]) -> bool:

        for i, rows in enumerate(board):
            rows_set = set()
            columns_set = set()
            block_set = set()

            row_bump = -1
            for j, num in enumerate(rows):
                number_check = num

                if number_check in rows_set and number_check != '.':
                    return False
                rows_set.add(number_check)

                number_check = board[j][i]

                if number_check in columns_set and number_check != '.':
                    return False
                columns_set.add(number_check)

                if j % 3 == 0:
                    row_bump += 1

                # I'm sorry for what you are about to experience here.... This was just terrible
                number_check = board[(int(i/3) * 3) + row_bump][(j % 3 + ((i * 3) % 9)) % (3 + ((i * 3) % 9))]

                if number_check in block_set and number_check != '.':
                    return False
                block_set.add(number_check)

        return True


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    board = [["5", "3", ".", ".", "7", ".", ".", ".", "."]
        , ["6", ".", ".", "1", "9", "5", ".", ".", "."]
        , [".", "9", "8", ".", ".", ".", ".", "6", "."]
        , ["8", ".", ".", ".", "6", ".", ".", ".", "3"]
        , ["4", ".", ".", "8", ".", "3", ".", ".", "1"]
        , ["7", ".", ".", ".", "2", ".", ".", ".", "6"]
        , [".", "6", ".", ".", ".", ".", "2", "8", "."]
        , [".", ".", ".", "4", "1", "9", ".", ".", "5"]
        , [".", ".", ".", ".", "8", ".", ".", "7", "9"]]
    expected = True

    # Setting up part 🏗
    answer = Solution().isValidSudoku(board)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
