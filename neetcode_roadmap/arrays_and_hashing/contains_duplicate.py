# https://leetcode.com/problems/contains-duplicate/
# Solved in 00:07:53
from typing import List
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        num_set = set()
        for num in nums:
            if num not in num_set:
                num_set.add(num)
            else:
                return True


        return False


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    question_input = [1,1,1,3,3,4,3,2,4,2]
    expected = True

    # Setting up part 🏗
    answer = Solution().containsDuplicate(question_input)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
