# https://leetcode.com/problems/longest-consecutive-sequence/
# Solved in about 15 minutes
# Leave notes for Mediums and Highs please

class Solution:
    def longestConsecutive(self, nums: list[int]) -> int:
        nums = list(set(nums))
        nums.sort()
        print(nums)
        longest_consecutive = 0
        current_length = 0

        if len(nums) == 0:
            return longest_consecutive

        for i in range(1, len(nums)):

            if nums[i] - nums[i - 1] == 1:
                current_length += 1
            else:
                longest_consecutive = max(current_length + 1, longest_consecutive)
                current_length = 0

        return max(current_length + 1, longest_consecutive)

        # If you wanted to not use sort, you could have gone the way of a set and instead loop through the
        # list, check to see if no number is already being accounted for being consecutive (n-1), and keep going up
        # until you reach a stopping point in the set where you can't continue
        #         longest = 0
        #         num_set = set(nums)
        #
        #         for n in num_set:
        #             if (n-1) not in num_set:
        #                 length = 1
        #                 while (n+length) in num_set:
        #                     length += 1
        #                 longest = max(longest, length)
        #
        #         return longest


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    nums = [0, 3, 7, 2, 5, 8, 4, 6, 0, 1]
    expected = 9

    # Setting up part 🏗
    answer = Solution().longestConsecutive(nums)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
