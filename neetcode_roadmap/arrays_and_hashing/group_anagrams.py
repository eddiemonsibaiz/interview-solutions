# https://leetcode.com/problems/group-anagrams/
# Solved in 00:32:36
# Leave notes for Medium and Highs please


class Solution:
    def groupAnagrams(self, strs: list[str]) -> list[list[str]]:

        # Short circuit when there is only one value in the strs list
        if len(strs) == 1:
            return [strs]

        # Sorting all words to quickly make out if a word is an anagram
        sorted_words = []

        for i, word in enumerate(strs):
            # This is such a overpowered tool to me (sorted()) Is this even valid for interview?
            # Here's my two cents. Ask the interviewer if using the built-in function is okay to use. Most
            # of the time it should be fine if the question is not upon your own knowledge of sorting.
            # Although, when going through explanation of what sorted does, it's a variant of Tim Sort
            # https://en.wikipedia.org/wiki/Timsort
            sorted_words.insert(i, ''.join(sorted(word)))

        # Create a dictionary of anagrams to group indexes of the strs array.
        anagram_dict = {}

        for i, word in enumerate(sorted_words):
            if word in anagram_dict:
                anagram_dict[word].insert(0, i)
            else:
                anagram_dict[word] = [i]

        # Read through each anagram and group them in an array
        results = []

        for anagram_index_list in anagram_dict.values():
            anagrams = []
            for i_result in anagram_index_list:
                anagrams = anagrams + [strs[i_result]]

            results += [anagrams]

        return results


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
    expected = [["bat"], ["nat", "tan"], ["ate", "eat", "tea"]]

    # Setting up part 🏗
    answer = Solution().groupAnagrams(strs)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
