# https://leetcode.com/problems/product-of-array-except-self/
# Solved in 00:40:02
# Leave notes for Mediums and Highs please


class Solution:

    def productExceptSelf(self, nums: list[int]) -> list[int]:
        length = len(nums)
        results = [1] * length

        left_tracker = 1
        right_tracker = 1
        for i in range(length):
            # Set left tracker to results first then multiply current number
            results[i] *= left_tracker
            left_tracker *= nums[i]

            i2 = length - 1 - i

            # Set right tracker to results first then multiply current number
            results[i2] *= right_tracker
            right_tracker *= nums[i2]

        return results

    def productExceptSelfOriginal(self, nums: list[int]) -> list[int]:
        # Calculate right side array
        right_first_nums = []
        for i, num in enumerate(nums):
            if i == 0:
                right_first_nums.append(num)
            else:
                right_first_nums.append(num * right_first_nums[i - 1])

        # Calculate left side array
        left_first_nums = []
        nums.reverse()
        for i, num in enumerate(nums):
            if i == 0:
                left_first_nums.append(num)
            else:
                left_first_nums.append(num * left_first_nums[i - 1])
        left_first_nums.reverse()

        # Combine everything but itself
        results = []
        for i in range(len(nums)):
            if i == 0:
                results.append(left_first_nums[i + 1])
            elif i == (len(nums) - 1):
                results.append(right_first_nums[i - 1])
            else:
                results.append(left_first_nums[i + 1] * right_first_nums[i - 1])

        return results


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    nums = [1, 2, 3, 4]
    expected = [24, 12, 8, 6]

    # Setting up part 🏗
    answer = Solution().productExceptSelf(nums)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
