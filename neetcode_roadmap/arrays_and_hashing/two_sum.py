# https://leetcode.com/problems/two-sum/
# Solved in estimated 8 minutes


class Solution:
    def twoSum(self, nums: list[int], target: int) -> list[int]:
        # key will be the number that is needed to make the sum, value will be the index of that number
        num_dict = {}

        for i in range(len(nums)):
            if nums[i] in num_dict:
                return [num_dict[nums[i]], i]
            else:
                num_dict[target - nums[i]] = i

        return [0,0]


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    nums = [3,3]
    target = 6
    expected = [0,1]

    # Setting up part 🏗
    answer = Solution().twoSum(nums,target)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
