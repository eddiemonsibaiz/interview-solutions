# https://leetcode.com/problems/two-sum/
# Solved in 00:39:53
# Leave notes for Mediums and Highs please


class Solution:

    def topKFrequent(self, nums: list[int], k: int) -> list[int]:
        # Made by another user
        dic = {}
        for i in nums:
            dic[i] = 1 + dic.get(i, 0)
        print(dic)
        # https://stackoverflow.com/questions/13669252/what-is-lambda-in-python-code-how-does-it-work-with-key-arguments-to-sorte
        # Take this into account for future questions
        sortdic = (sorted(dic.items(), key=lambda item: item[1]))
        print(sortdic)
        l = []
        j = -1
        for i in range(k):
            l.append(sortdic[j][0])
            j -= 1
        return l

    # What I learned in boating school today: remember that removing for the sake of sorting/searching is never a good
    # idea. You waste runtime by having to do garbage collection. Try to do in place manipulation at least, where you
    # are using your garbage for something useful. Think of it as recycling a list and repurposing that for a better
    # solution.

    def topKFrequentOriginal(self, nums: list[int], k: int) -> list[int]:
        nums_dict = {}

        # Put all numbers as the key into a dict
        # For each number you find, put the occurrence as the value
        for num in nums:
            if num not in nums_dict:
                nums_dict[num] = 1
            else:
                nums_dict[num] += 1

        # Go through the hashmap values, and get the max, then take out that key value pair and do the next one for the
        # next k times

        num_keys = list(nums_dict.keys())
        num_value = list(nums_dict.values())
        results = []

        for i in range(k):
            max_num_val = max(nums_dict.values())
            position = num_value.index(max_num_val)
            results.insert(0, num_keys[position])

            # Clean up
            nums_dict.pop(num_keys[position])
            num_keys.pop(position)
            num_value.pop(position)

        return results


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    nums = [1,2]
    k = 2
    expected = [1,2]

    # Setting up part 🏗
    answer = Solution().topKFrequent(nums, k)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
