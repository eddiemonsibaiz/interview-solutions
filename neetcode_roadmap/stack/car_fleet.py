# https://leetcode.com/problems/car-fleet/
# NOT Solved in 01:33:08. Probably took me 3 hours 🙄
# Leave notes for Mediums and Highs please
import time


class Solution:
    def carFleet(self, target: int, position: list[int], speed: list[int]) -> int:
        fleets = 1

        sorted_cars = sorted(list(zip(position, speed)))

        # What I find interesting with this problem is that popping is better than a array pointer in terms of runtime
        # What is the lesson in all this 🤷‍♂️
        pos, speed = sorted_cars.pop()

        fleet_max_speed = (target - pos) / speed
        while sorted_cars:
            pos, speed = sorted_cars.pop()
            check = (target - pos) / speed
            if check > fleet_max_speed:
                fleets += 1
                fleet_max_speed = check

        return fleets





# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    target = 10
    position = [8,3,7,4,6,5]
    speed = [4,4,4,4,4,4]

    # target = 12
    # position = [10,8,0,5,3]
    # speed = [2,4,1,1,3]

    expected = 3

    # Setting up part 🏗

    start_time = time.time()
    answer = Solution().carFleet(target, position, speed)
    end_time = time.time()

    print("🦘 Question:\t " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer:\t\t " + str(answer))
    print("🦆 Expected:\t " + str(expected))
    print("🦥 Elapsed Time: " + str(round(end_time - start_time, 3)) + " seconds")


answer_result()
