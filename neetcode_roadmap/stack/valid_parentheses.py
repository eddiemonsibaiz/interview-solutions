# https://leetcode.com/problems/valid-parentheses/
# Solved in 00:11:21
# Leave notes for Mediums and Highs please


class Solution:
    def isValid(self, s: str) -> bool:
        parentheses_dict = {'(': ')', '[': ']', '{': '}'}
        seen_stack = []

        for i in range(len(s)):
            if s[i] in parentheses_dict:
                seen_stack.append(s[i])
            else:
                if not seen_stack:
                    return False
                if parentheses_dict[seen_stack[-1]] != s[i]:
                    return False
                seen_stack.pop()

        return not seen_stack


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    input = "([])[]"
    expected = True

    # Setting up part 🏗
    answer = Solution().isValid(input)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))
