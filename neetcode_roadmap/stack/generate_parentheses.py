# https://leetcode.com/problems/generate-parentheses/
# Solved in 01:19:27 Stopped at 00:51:31
# Leave notes for Mediums and Highs please


class Solution:
    def __init__(self):
        self.results = []

    # This is a better solution than the previous answer due to the fact that we aren't constantly copying and
    # referencing a list the whole time. The main idea on this problem is to look at this problem through the lens of
    # a stack, but without necessarily implementing a stack. I thought that was pretty neat, and I'll keep that in mind
    # for other data structure problems
    def generateParenthesis(self, n: int) -> list[str]:
        self.generate2(n, n, "")
        return self.results


    def generate2(self, open_parentheses_num, closed_parentheses_num, result):
        if open_parentheses_num == closed_parentheses_num == 0:
            self.results.append(result)
            return

        if closed_parentheses_num != 0 and open_parentheses_num < closed_parentheses_num:
            self.generate2(open_parentheses_num, closed_parentheses_num - 1, result + ")")

        if open_parentheses_num != 0:
            self.generate2(open_parentheses_num - 1, closed_parentheses_num, result + "(")


    def generateParenthesisOriginal(self, n: int) -> list[str]:
        open_parentheses_stack = ["("] * n
        closed_parentheses_stack = [")"] * n

        self.generate(open_parentheses_stack, closed_parentheses_stack, "")

        return self.results

    def generate(self, open_parentheses_stack, closed_parentheses_stack, result):
        if open_parentheses_stack == closed_parentheses_stack:
            self.results.append(result)
            return

        if len(closed_parentheses_stack) != 0 and len(open_parentheses_stack) < len(closed_parentheses_stack):
            self.generate(open_parentheses_stack, closed_parentheses_stack[:-1], result + closed_parentheses_stack[-1])

        if len(open_parentheses_stack) != 0:
            self.generate(open_parentheses_stack[:-1], closed_parentheses_stack, result + open_parentheses_stack[-1])


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    n = 4
    expected = ["(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())", "(()())()", "(())(())",
                "(())()()", "()((()))", "()(()())", "()(())()", "()()(())", "()()()()"]
    # n = 2
    # expected = ["(())", "()()"]

    # Setting up part 🏗
    answer = Solution().generateParenthesis(n)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))


answer_result()
