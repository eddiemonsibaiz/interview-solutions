# https://leetcode.com/problems/evaluate-reverse-polish-notation/
# Solved in 00:19:13
# Leave notes for Mediums and Highs please


def add_func(num1, num2):
    return num1 + num2


def minus_func(num1, num2):
    return num1 - num2


def multiple_func(num1, num2):
    return num1 * num2


def divide_func(num1, num2):
    return num1 / num2


class Solution:

    def evalRPN(self, tokens: list[str]) -> int:
        # Interesting that you can assign these values to a function
        valid_operators_dict = {"+": add_func, "-": minus_func, "*": multiple_func, "/": divide_func}

        stack = []
        for token in tokens:
            if token in valid_operators_dict:
                num2 = int(stack.pop())
                num1 = int(stack.pop())
                stack.append(valid_operators_dict[token](num1, num2))
            else:
                stack.append(token)

        return int(stack[0])


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
    expected = 22

    # Setting up part 🏗
    answer = Solution().evalRPN(tokens)
    print("🦘 Question: " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer: " + str(answer))
    print("🦆 Expected: " + str(expected))


answer_result()
