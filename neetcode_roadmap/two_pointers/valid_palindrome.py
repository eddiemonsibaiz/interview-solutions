# https://leetcode.com/problems/valid-palindrome/
# Solved in about 15 minutes
# Leave notes for Mediums and Highs please
import re
import time


class Solution:

    def isPalindrome(self, s: str) -> bool:
        s = re.sub("[^A-Za-z0-9]", "", s.lower())
        pointer = 0
        while s and pointer <= len(s) // 2:
            if s[pointer] != s[-1 - pointer]:
                return False
            pointer += 1

        return True

    # def isPalindrome(self, s: str) -> bool:
    #
    #     forward_s = ""
    #     backward_s = ""
    #     s = s.lower()
    #
    #     for i in range(len(s)):
    #         temp = s[i]
    #         if temp.isalpha() or temp.isnumeric():
    #             forward_s += temp
    #
    #         temp = s[-1 - i]
    #         if temp.isalpha() or temp.isnumeric():
    #             backward_s += temp
    #
    #     return forward_s == backward_s


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    s = " "
    expected = True

    # Setting up part 🏗

    start_time = time.time()
    answer = Solution().isPalindrome(s)
    end_time = time.time()

    print("🦘 Question:\t " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer:\t\t " + str(answer))
    print("🦆 Expected:\t " + str(expected))
    print("🦥 Elapsed Time: " + str(round(end_time - start_time, 3)) + " seconds")


answer_result()
