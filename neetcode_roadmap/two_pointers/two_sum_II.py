# https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
# Solved in about 00:21:00 Not bad!
# Leave notes for Mediums and Highs please
import time


class Solution:
    # This was a simple solution. You start the pointers on both sides of the array. If you add the left side and the
    # right side together and noticed that the result is too high, you wouldn't want to move your left pointer up
    # since that will only make your result go higher. No, you would want to have the right pointer go down so that
    # way you can lower the result to the target, and vise versa if the result is too low. If you are wondering why I
    # organized the if condition this way, it's because I ordered it from most likely to be true to least likely.
    #
    # One optimization I see here is to instead do the addition only once instead on both if conditions.
    def twoSum(self, numbers: list[int], target: int) -> list[int]:
        left = 0
        right = -1
        while True:
            if numbers[left] + numbers[right] > target:
                right -= 1
            elif numbers[left] + numbers[right] < target:
                left += 1
            else:
                return [left + 1, len(numbers) + right + 1]




# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    numbers = [2,7,11,15]
    target = 9
    expected = [1,2]

    # Setting up part 🏗

    start_time = time.time()
    answer = Solution().twoSum(numbers, target)
    end_time = time.time()

    print("🦘 Question:\t " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer:\t\t " + str(answer))
    print("🦆 Expected:\t " + str(expected))
    print("🦥 Elapsed Time: " + str(round(end_time - start_time, 3)) + " seconds")


answer_result()
