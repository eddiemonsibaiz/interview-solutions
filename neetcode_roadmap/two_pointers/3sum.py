# https://leetcode.com/problems/3sum/
# Gave up in 01:41:31
# Leave notes for Mediums and Highs please
import time


class Solution:

    def threeSum(self, nums: list[int]) -> list[list[int]]:
        nums.sort()
        results = []

        # Basically 2Sum but with extra steps 💁‍♂️ This was something else I tell you
        for i, target in enumerate(nums):
            if i > 0 and nums[i] == nums[i - 1]:
                continue

            left = i + 1
            right = len(nums) - 1

            while left < right:
                total = nums[left] + nums[right] + target
                if total > 0:
                    right -= 1
                elif total < 0:
                    left += 1
                else:
                    results.append([target, nums[left], nums[right]])
                    left += 1
                    while nums[left] == nums[left - 1] and left < right:
                        left += 1

        return results

    # Failed attempt
    # def threeSum(self, nums: list[int]) -> list[list[int]]:
    #     nums.sort()
    #     left = 0
    #     right = len(nums) - 1
    #     seen = set()
    #     results = []
    #
    #     while left < right:
    #         third_num = -(nums[right] + nums[left])
    #
    #         if third_num in set(nums[left + 1:right]) and (nums[right] not in seen and nums[left] not in seen):
    #             results.append([nums[right], nums[left], third_num])
    #
    #         if third_num == 0 and third_num in set(nums[left + 1:right - 1]) and (nums[right] not in seen and nums[left] not in seen):
    #
    #         if third_num <= 0:
    #             seen.add(nums[right])
    #             right -= 1
    #         else:
    #             seen.add(nums[left])
    #             left += 1
    #
    #     return results


# The answer_result will be used to build the question and feed it into your solution.
# This function is made so that you won't have to worry about creating a Solution object and setting all that
# up in main (You're welcome 😛)
def answer_result():
    # This is what your solution will be based on 🍫
    # nums = [-1,0,1,2,-1,-4]
    # nums = [-2, 0, 1, 1, 2]
    # expected = [[-2,0,2],[-2,1,1]]
    # nums = [-1,0,1,2,-1,-4]
    # expected = [[-1,-1,2],[-1,0,1]]
    nums = [-1, 0, 1, 2, -1, -4, -2, -3, 3, 0, 4]
    expected = [[-4, 0, 4], [-4, 1, 3], [-3, -1, 4], [-3, 0, 3], [-3, 1, 2], [-2, -1, 3], [-2, 0, 2], [-1, -1, 2],
                [-1, 0, 1]]

    # Setting up part 🏗

    start_time = time.time()
    answer = Solution().threeSum(nums)
    end_time = time.time()

    print("🦘 Question:\t " + str(str(__file__).split("\\")[-1]))
    print("🦜 Answer:\t\t " + str(answer))
    print("🦆 Expected:\t " + str(expected))
    print("🦥 Elapsed Time: " + str(round(end_time - start_time, 3)) + " seconds")


answer_result()
